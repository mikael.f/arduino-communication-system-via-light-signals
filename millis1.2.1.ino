//--------------------------------------------------------------------------------------VARIAVEIS GERAIS------------------------------------------------------------------------------------
int luz = 7;
//usada para definir porta usada para emitir o sinal de luz

int mikael = 5;
//unidade de medida de tempo para morse

int ponto = mikael, traco = mikael * 3, nada = mikael * 5;
//usadas como unidades no sistema morse adaptado

char letra;

//-----------------------------------------------------------------------------------VARIAVEIS/FUNCOES PARA TRANMISSÃO------------------------------------------------------------------------
unsigned long envioEvento;
//usada para marcar o tempo de Eventos relacionados a transmissao de luz

unsigned long tempoQueDesligou = 0;
//usada na funcao digito para determinar quando a luz deixo de ser emitida

bool setTQD = true;
//usada para controlar quando a variavel tempoQueDesligou receberá valor

bool novoEnvioEvento = true;
//usada para controlar quando a variavel envioEvento receberá novo valor

String msg[31];
//array bidimensional que contera valores inteiros relativos aos caracteres da mensagem do usuário

int msgCont = 0, apagarCont = 31, letraCont = 0, digitoCont = 0;
//usadas para operações dentro do array. msgCont = usada para adicionar letras ao array. apagarCont = usada para resetar o array
//letraCont = usada para navegar dentre as letras do array. digitoCont = usada para navegar dentro dos caracteres das letras do array

unsigned long apagarEvento = 0;
//usada para guardar qual o envioEvento associado a inatividade atual

bool inativo = true;
//usada na lógica de reset do array por inatividade
//---------------------------------------------------------------------------------------VARIAVIES/FUNCOES PARA RECEPÇÃO-----------------------------------------------------------------
int LDR = A0;
//variavel que define a porta onde esta o sensor LDR

int leituraLDR;
//variavel que sera usada para armazenar a leitura do LDR

int setPoint = 600;
//variavel usada para definir se o LDR esta ou não recebendo sinais de luz

bool interpretar = false, setMF = false, setMI = true;
//usadas na logica de processamento dos sinais recebidos no LDR. interpretar =  informa ao programa que pode associar um tempo decorrido a um digito
//setarMI = usada para controlar quando a variavel momentoInicial receberá novo valor. setarMF = usada para controlar quando a variavel momentoFinal receberá novo valor.

unsigned long recepEvento, momentoInicial, momentoFinal = 0;
//recepEvento = usada para marcar o tempo de Eventos relacionados a recepcao de luz. momentoInicial = usada para arquivar momento em que o LDR detecta luz
//momentoFinal = usada para arquivar o momento em que a luz que estava sendo recebida cessa

int cmorse[5];
//usada na lógica de interpretação das letras. Armazena cada digito relativo a uma letra

int contCmorse;
//usada para operação dentro do array cmorse
//-------------------------------------------------------------------- FUNÇÕES ----------------------------------------------------------------------------------------------------

//função que emite 1 digito(ponto, traço ou nada). É chamada redefinindo o valor de novoEnvioEvento
int digito(unsigned long envioEvento, int digitoMorse) {
  if (millis() - envioEvento >= digitoMorse) {
    digitalWrite(luz, LOW);
    if (setTQD) {
      tempoQueDesligou = millis();
      setTQD = false;
    }
    if (millis() - tempoQueDesligou >= mikael) {
      setTQD = true;
      return true;//só saimos da funcao apos o tempo do digito + uma unidade de espera entre o proximo digito
    }
  } else {
    digitalWrite(luz, HIGH);
  }
  return false;
}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(luz, OUTPUT);
}

void loop() {

  //--------------------------------------------------------------------------------------------TRANSMISSAO-----------------------------------------------------------------------
  letra = Serial.read();
  if (msg[letraCont][digitoCont] == '*') {
    //novoEnvioEvento sendo utilizada para armazenar em envioEvento apenas o momento em que a luz começará a ser emitida
    if (novoEnvioEvento) {
      envioEvento = millis();
      novoEnvioEvento = false;//impede que envioEvento seja redefinido varias vezes para o mesmo digito
    }
    novoEnvioEvento = digito(envioEvento, ponto);
    if (novoEnvioEvento) {
      digitoCont++; //como a emissão do digito terminou, passamos para o proximo
    }
  }

  if (msg[letraCont][digitoCont] == '-') {
    if (novoEnvioEvento) {
      envioEvento = millis();
      novoEnvioEvento = false;
    }
    novoEnvioEvento = digito(envioEvento, traco);
    if (novoEnvioEvento) {
      digitoCont++;
    }
  }

  if (msg[letraCont][digitoCont] == 'x') {
    if (novoEnvioEvento) {
      envioEvento = millis();
      novoEnvioEvento = false;
    }
    novoEnvioEvento = digito(envioEvento, nada);
    if (novoEnvioEvento) {
      digitoCont = 0;
      letraCont++;//adicionamos 1 a letraCont e zeramos digitoCont pois sabemos que a letra acabou
    }
  }

  //caso a letra não termine em nada (2 ou X), é aqui que o fluxo reinicia para a proxima letra
  if (digitoCont == 5) {
    letraCont++;
    digitoCont = 0;
  }

  //reset do array e das variaveis letraCont e msgCont caso não haja envio em certo periodo de tempo. envioEvento != apagarEvento certifica que o array só sera resetado apos
  //a inatividade atual ter sido passada
  if (millis() - envioEvento >= 200 && envioEvento != apagarEvento) {
    apagarEvento = envioEvento;//salvando qual o envioEvento associado a inatividade atual
    apagarCont = 0;
  }
  if (apagarCont <= 30) {
    inativo = false;//evita que o if anterior zere apagarCont
    letraCont = 0;
    msgCont = 0;
    msg[apagarCont] = "";//zerando o array
    apagarCont++;
  } else {
    inativo = true;//restaura a possibilidade de reset
  }

  if (msgCont == 30) {
    msgCont = 0;//zerando msgCont caso ele chegue ao limite do array
    letraCont = 0;
  }
  //---------------------------------------------------- Processo de checagem dos caracteres e adição ao array msg ---------------------------------------------------------------------
  //Serial.println(msg[0]);
  //Serial.println(msgCont);
  if (letra == ' ') {
    msg[msgCont] = "x";
    msgCont++;
  }
  if (letra == 'a') {
    msg[msgCont] = "*-***";
    msgCont++;
  }
  if (letra == 'b') {
    msg[msgCont] =  "*-*-*";
    msgCont++;
  }
  if (letra == 'c') {
    msg[msgCont] = "*-**x";
    msgCont++;
  }
  if (letra == 'd') {
    msg[msgCont] = "**x";
    msgCont++;
  }
  if (letra == 'e') {
    msg[msgCont] = "***-*";
    msgCont++;
  }
  if (letra == 'f') {
    msg[msgCont] = "*--*x";
    msgCont++;
  }

  if (letra == 'g') {
    msg[msgCont] = "*****";
    msgCont++;
  }

  if (letra == 'h') {
    msg[msgCont] = "***x";
    msgCont++;
  }
  if (letra == 'i') {
    msg[msgCont] = "**---";
    msgCont++;
  }
  if (letra == 'j') {
    msg[msgCont] = "*-*-x";
    msgCont++;
  }
  if (letra == 'k') {
    msg[msgCont] = "**-**";
    msgCont++;
  }
  if (letra == 'l') {
    msg[msgCont] = "*--x";
    msgCont++;
  }
  if (letra == 'm') {
    msg[msgCont] = "*-*x";
    msgCont++;
  }
  if (letra == 'n') {
    msg[msgCont] = "*---x";
    msgCont++;
  }
  if (letra == 'o') {
    msg[msgCont] = "**--*";
    msgCont++;
  }
  if (letra == 'p') {
    msg[msgCont] = "*--*-";
    msgCont++;
  }
  if (letra == 'q') {
    msg[msgCont] = "**-*x";
    msgCont++;
  }
  if (letra == 'r') {
    msg[msgCont] = "****x";
    msgCont++;
  }
  if (letra == 's') {
    msg[msgCont] = "*-x";
    msgCont++;
  }
  if (letra == 't') {
    msg[msgCont] = "***-x";
    msgCont++;
  }
  if (letra == 'u') {
    msg[msgCont] = "****-";
    msgCont++;
  }
  if (letra == 'v') {
    msg[msgCont] = "**--x";
    msgCont++;
  }
  if (letra == 'w') {
    msg[msgCont] = "*-**-";
    msgCont++;
  }
  if (letra == 2) {
    msg[msgCont] = "*-*--";
    msgCont++;
  }
  if (letra == 'y') {
    msg[msgCont] = "*--**";
    msgCont++;
  }

  if (letra == 'z') {
    msg[msgCont] = "**-x";
    msgCont++;
  }
  //--------------------------------------------------------------------------- LETRAS MAIUSCULAS -------------------------------------------------------------------------------------
  if (letra == 'A') {
    msg[msgCont] = "--***";
    msgCont++;
  }
  if (letra == 'B') {
    msg[msgCont] =  "--*-*";
    msgCont++;
  }
  if (letra == 'C') {
    msg[msgCont] = "--**x";
    msgCont++;
  }
  if (letra == 'D') {
    msg[msgCont] = "-*x";
    msgCont++;
  }
  if (letra == 'E') {
    msg[msgCont] = "-**-*";
    msgCont++;
  }
  if (letra == 'F') {
    msg[msgCont] = "---*x";
    msgCont++;
  }

  if (letra == 'G') {
    msg[msgCont] = "-****";
    msgCont++;
  }

  if (letra == 'H') {
    msg[msgCont] = "-**x";
    msgCont++;
  }
  if (letra == 'I') {
    msg[msgCont] = "-*---";
    msgCont++;
  }
  if (letra == 'J') {
    msg[msgCont] = "--*-x";
    msgCont++;
  }
  if (letra == 'K') {
    msg[msgCont] = "-*-**";
    msgCont++;
  }
  if (letra == 'L') {
    msg[msgCont] = "---x";
    msgCont++;
  }
  if (letra == 'M') {
    msg[msgCont] = "--*x";
    msgCont++;
  }
  if (letra == 'N') {
    msg[msgCont] = "----x";
    msgCont++;
  }
  if (letra == 'O') {
    msg[msgCont] = "-*--*";
    msgCont++;
  }
  if (letra == 'P') {
    msg[msgCont] = "---*-";
    msgCont++;
  }
  if (letra == 'Q') {
    msg[msgCont] = "-*-*x";
    msgCont++;
  }
  if (letra == 'R') {
    msg[msgCont] = "-***x";
    msgCont++;
  }
  if (letra == 'S') {
    msg[msgCont] = "--x";
    msgCont++;
  }
  if (letra == 'T') {
    msg[msgCont] = "-**-x";
    msgCont++;
  }
  if (letra == 'U') {
    msg[msgCont] = "-***-";
    msgCont++;
  }
  if (letra == 'V') {
    msg[msgCont] = "-*--x";
    msgCont++;
  }
  if (letra == 'W') {
    msg[msgCont] = "--**-";
    msgCont++;
  }
  if (letra == 2) {
    msg[msgCont] = "--*--";
    msgCont++;
  }
  if (letra == 'Y') {
    msg[msgCont] = "---**";
    msgCont++;
  }
  if (letra == 'Z') {
    msg[msgCont] = "-*-x";
    msgCont++;
  }

  //-----------------------------------------------------------------------------------------------RECEPCAO-------------------------------------------------------------------------------
  leituraLDR = analogRead(LDR);
  //Serial.println(leituraLDR);

  if (leituraLDR <= setPoint) {
    if (setMI) {
      momentoInicial = millis();
      setMI = false;//armazenamos em momentoInicial apenas o momento em que o sinal é percebido
    }
  } else if (!setMI) {
    setMF = true;//so pode haver um valor para momentoFinal se tiver havido novo valor para momentoInicial
  }
  if (setMF) {
    momentoFinal = millis();
    interpretar = true;//o sinal so pode ser interpretado se os valores de momentoInicial e momentoFinal tiverem sido renovados
    setMF = false;
    setMI = true;//permite novo momentoInicial em momento futuro
  }
  if (interpretar) {
    int tempoAceso = momentoFinal - momentoInicial;//tempo em que a luz foi percebido
    if (tempoAceso >= ponto - 4 && tempoAceso < traco - 4) {//operações de menos são margem de erro
      cmorse[contCmorse] = 0;
      contCmorse++;//colocamos o digito em especifico no array cmorse e adicionamos a contCmorse para que o proximo digito possa ser colocado
    } else if (tempoAceso >= traco - 4 && tempoAceso < nada - 4) {
      cmorse[contCmorse] = 1;
      contCmorse++;
    } else if (tempoAceso >= nada - 4 ) {
      cmorse[contCmorse] = 2;
      contCmorse = 5;//igualamos contCmorse a 5 para fechar o ciclo pois sabemos que o digito acabou
    }
    interpretar = false;//impede que a mesma letra seja interpretada continuadamente
  }
  //com contCmorse igual a 5 sabemos que o digito terminou e podemos descobrir qual letra recebemos
  if (contCmorse == 5) {
    contCmorse = 0;
    if ( cmorse[0] == 0 && cmorse[1] == 1 && cmorse[2] == 0 && cmorse[3] == 0 && cmorse[4] == 0) {
      Serial.print("a");
    }
    if (cmorse[0] == 0 && cmorse[1] == 1 && cmorse[2] == 0 && cmorse[3] == 1 && cmorse[4] == 0) {
      Serial.print("b");
    }
    if (cmorse[0] == 0 && cmorse[1] == 1 && cmorse[2] == 0 && cmorse[3] == 0 && cmorse[4] == 2) {
      Serial.print("c");
    }
    if ( cmorse[0] == 0 && cmorse[1] == 0 && cmorse[2] == 2) {
      Serial.print("d");
    }
    if ( cmorse[0] == 0 && cmorse[1] == 0 && cmorse[2] == 0 && cmorse[3] == 1 && cmorse[4] == 0) {
      Serial.print("e");
    }
    if ( cmorse[0] == 0 && cmorse[1] == 1 && cmorse[2] == 1 && cmorse[3] == 0 && cmorse[4] == 2) {
      Serial.print("f");
    }
    if (  cmorse[0] == 0 && cmorse[1] == 0 && cmorse[2] == 0 && cmorse[3] == 0 && cmorse[4] == 0) {
      Serial.print("g");
    }
    if ( cmorse[0] == 0 && cmorse[1] == 0 && cmorse[2] == 0 && cmorse[3] == 2) {
      Serial.print("h");
    }
    if (  cmorse[0] == 0 && cmorse[1] == 0 && cmorse[2] == 1 && cmorse[3] == 1 && cmorse[4] == 1) {
      Serial.print("i");
    }
    if ( cmorse[0] == 0 && cmorse[1] == 1 && cmorse[2] == 0 && cmorse[3] == 1 && cmorse[4] == 2) {
      Serial.print("j");
    }
    if ( cmorse[0] == 0 && cmorse[1] == 0 && cmorse[2] == 1 && cmorse[3] == 0 && cmorse[4] == 0) {
      Serial.print("k");
    }
    if ( cmorse[0] == 0 && cmorse[1] == 1 && cmorse[2] == 1 && cmorse[3] == 2) {
      Serial.print("l");
    }
    if (cmorse[0] == 0 && cmorse[1] == 1 && cmorse[2] == 0 && cmorse[3] == 2) {
      Serial.print("m");
    }
    if ( cmorse[0] == 0 && cmorse[1] == 1 && cmorse[2] == 1 && cmorse[3] == 1 && cmorse[4] == 2) {
      Serial.print("n");
    }
    if ( cmorse[0] == 0 && cmorse[1] == 0 && cmorse[2] == 1 && cmorse[3] == 1 && cmorse[4] == 0) {
      Serial.print("o");
    }
    if (  cmorse[0] == 0 && cmorse[1] == 1 && cmorse[2] == 1 && cmorse[3] == 0 && cmorse[4] == 1) {
      Serial.print("p");
    }
    if ( cmorse[0] == 0 && cmorse[1] == 0 && cmorse[2] == 1 && cmorse[3] == 0 && cmorse[4] == 2) {
      Serial.print("q");
    }
    if ( cmorse[0] == 0 && cmorse[1] == 0 && cmorse[2] == 0 && cmorse[3] == 0 && cmorse[4] == 2) {
      Serial.print("r");
    }
    if ( cmorse[0] == 0 && cmorse[1] == 1 && cmorse[2] == 2) {
      Serial.print("s");
    }
    if (  cmorse[0] == 0 && cmorse[1] == 0 && cmorse[2] == 0 && cmorse[3] == 1 && cmorse[4] == 2) {
      Serial.print("t");
    }
    if ( cmorse[0] == 0 && cmorse[1] == 0 && cmorse[2] == 0 && cmorse[3] == 0 && cmorse[4] == 1) {
      Serial.print("u");
    }
    if ( cmorse[0] == 0 && cmorse[1] == 0 && cmorse[2] == 1 && cmorse[3] == 1 && cmorse[4] == 2) {
      Serial.print("v");
    }
    if ( cmorse[0] == 0 && cmorse[1] == 1 && cmorse[2] == 0 && cmorse[3] == 0 && cmorse[4] == 1) {
      Serial.print("w");
    }
    if ( cmorse[0] == 0 && cmorse[1] == 1 && cmorse[2] == 0 && cmorse[3] == 1 && cmorse[4] == 1) {
      Serial.print("x");
    }
    if (cmorse[0] == 0 && cmorse[1] == 1 && cmorse[2] == 1 && cmorse[3] == 0 && cmorse[4] == 0) {
      Serial.print("y");
    }
    if ( cmorse[0] == 0 && cmorse[1] == 0 && cmorse[2] == 1 && cmorse[3] == 2) {
      Serial.print("z");
    }

    if ( cmorse[0] == 1 && cmorse[1] == 1 && cmorse[2] == 0 && cmorse[3] == 0 && cmorse[4] == 0) {
      Serial.print("A");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 1 && cmorse[2] == 0 && cmorse[3] == 1 && cmorse[4] == 0) {
      Serial.print("B");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 1 && cmorse[2] == 0 && cmorse[3] == 0 && cmorse[4] == 2) {
      Serial.print("C");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 0 && cmorse[2] == 2) {
      Serial.print("D");
    }
    if (  cmorse[0] == 1 && cmorse[1] == 0 && cmorse[2] == 0 && cmorse[3] == 1 && cmorse[4] == 0) {
      Serial.print("E");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 1 && cmorse[2] == 1 && cmorse[3] == 0 && cmorse[4] == 2) {
      Serial.print("F");
    }
    if (cmorse[0] == 1 && cmorse[1] == 0 && cmorse[2] == 0 && cmorse[3] == 0 && cmorse[4] == 0) {
      Serial.print("G");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 0 && cmorse[2] == 0 && cmorse[3] == 2) {
      Serial.print("H");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 0 && cmorse[2] == 1 && cmorse[3] == 1 && cmorse[4] == 1) {
      Serial.print("I");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 1 && cmorse[2] == 0 && cmorse[3] == 1 && cmorse[4] == 2) {
      Serial.print("J");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 0 && cmorse[2] == 1 && cmorse[3] == 0 && cmorse[4] == 0) {
      Serial.print("K");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 1 && cmorse[2] == 1 && cmorse[3] == 2) {
      Serial.print("L");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 1 && cmorse[2] == 0 && cmorse[3] == 2) {
      Serial.print("M");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 1 && cmorse[2] == 1 && cmorse[3] == 1 && cmorse[4] == 2) {
      Serial.print("N");
    }
    if (cmorse[0] == 1 && cmorse[1] == 0 && cmorse[2] == 1 && cmorse[3] == 1 && cmorse[4] == 0) {
      Serial.print("O");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 1 && cmorse[2] == 1 && cmorse[3] == 0 && cmorse[4] == 1) {
      Serial.print("P");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 0 && cmorse[2] == 1 && cmorse[3] == 0 && cmorse[4] == 2) {
      Serial.print("Q");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 0 && cmorse[2] == 0 && cmorse[3] == 0 && cmorse[4] == 2) {
      Serial.print("R");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 1 && cmorse[2] == 2) {
      Serial.print("S");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 0 && cmorse[2] == 0 && cmorse[3] == 1 && cmorse[4] == 2) {
      Serial.print("T");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 0 && cmorse[2] == 0 && cmorse[3] == 0 && cmorse[4] == 1) {
      Serial.print("U");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 0 && cmorse[2] == 1 && cmorse[3] == 1 && cmorse[4] == 2) {
      Serial.print("V");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 1 && cmorse[2] == 0 && cmorse[3] == 0 && cmorse[4] == 1) {
      Serial.print("W");
    }
    if (  cmorse[0] == 1 && cmorse[1] == 1 && cmorse[2] == 0 && cmorse[3] == 1 && cmorse[4] == 1) {
      Serial.print("X");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 1 && cmorse[2] == 1 && cmorse[3] == 0 && cmorse[4] == 0) {
      Serial.print("Y");
    }
    if ( cmorse[0] == 1 && cmorse[1] == 0 && cmorse[2] == 1 && cmorse[3] == 2) {
      Serial.print("Z");
    }

    if ( cmorse[0] == 2) {
      Serial.print(" ");
    }
  }
}
